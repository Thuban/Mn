# Mn - MPD notifier

Mn is a simple python daemon showing "Now Playing" notifications.

It requires : 
- python >= 3.4
- python-tkinter
- python-PIL or python-Pillow
- python-mpd2

The project is now [here](http://dev.yeuxdelibad.net/tk-tools/Mn)

![screenshot of Mn](mn1.png)

## Configuration
Edit ``mn`` file to adjust : 

	MUSIC_DIR="/mnt/ZIC/"
	MESSAGE="♪ On air ♪"
	COVER_SIZE=(100,100)
	COVERS=["cover.jpg", "cover.JPG", "folder.jpg"]
	SLEEP=500 # in ms



